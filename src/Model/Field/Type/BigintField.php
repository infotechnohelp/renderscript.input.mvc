<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Field\Type;

use RenderScript\Input\MVC\Model\Field\Type;

class BigintField extends Type
{
    /**
     * @var string|null
     */
    protected $type = 'bigint';
}