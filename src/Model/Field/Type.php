<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Field;

use RenderScript\Input\MVC\Model\Field\Type\StringField;
use RenderScript\Input\MVC\Model\Field\Type\TextField;
use RenderScript\Input\MVC\Model\Field\Type\LongtextField;
use RenderScript\Input\MVC\Model\Field\Type\IntegerField;
use RenderScript\Input\MVC\Model\Field\Type\BigintField;
use RenderScript\Input\MVC\Model\Field\Type\FloatField;
use RenderScript\Input\MVC\Model\Field\Type\BooleanField;
use RenderScript\Input\MVC\Model\Field\Type\DateTimeField;

class Type
{
    /**
     * @var string|null
     */
    protected $type = null;

    /**
     * @return string
     */
    public function getTypeAsString(): string
    {
        return $this->type;
    }

    public static function string()
    {
        return new StringField();
    }

    public static function text()
    {
        return new TextField();
    }

    public static function longtext()
    {
        return new LongtextField();
    }


    public static function integer()
    {
        return new IntegerField();
    }


    public static function bigint()
    {
        return new BigintField();
    }

    public static function float()
    {
        return new FloatField();
    }

    public static function boolean()
    {
        return new BooleanField();
    }

    public static function datetime()
    {
        return new DateTimeField();
    }
}