<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Field;

class Config
{
    /**
     * @var int|null
     */
    private $length = null;

    /**
     * @var bool
     */
    private $nullable = false;

    /**
     * @var bool
     */
    private $unique = false;

    private $default = null;

    public function length(int $value): self
    {
        $this->length = $value;
        return $this;
    }

    public function nullable(bool $value = true): self
    {
        $this->nullable = $value;
        return $this;
    }

    public function unique(bool $value = true): self
    {
        $this->unique = $value;
        return $this;
    }

    public function default($value): self
    {
        $this->default = $value;
        return $this;
    }


    /**
     * @return int|null
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return bool
     */
    public function isNullable(): bool
    {
        return $this->nullable;
    }

    /**
     * @return bool
     */
    public function isUnique(): bool
    {
        return $this->unique;
    }

    /**
     * @return mixed|null
     */
    public function getDefault()
    {
        return $this->default;
    }

}