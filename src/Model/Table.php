<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model;

use RenderScript\Input\MVC\Model\Field\Config as FieldConfig;
use RenderScript\Input\MVC\Model\Field\Type as FieldType;
use RenderScript\Input\MVC\Model\Table\DatabaseConfig;
use RenderScript\Input\MVC\Model\Table\EntityConfig;
use RenderScript\Input\MVC\Model\Table\TableConfig;

class Table extends TableBase
{
    /**
     * @var array
     */
    private $relations = [];

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @var DatabaseConfig
     */
    private $databaseConfig;

    /**
     * @var EntityConfig
     */
    private $entityConfig;

    /**
     * @var TableConfig
     */
    private $tableConfig;

    public function __construct(string $title)
    {
        parent::__construct($title);

        $this->databaseConfig = new DatabaseConfig();
        $this->entityConfig = new EntityConfig();
        $this->tableConfig = new TableConfig();
    }

    /**
     * @return EntityConfig
     */
    public function getEntityConfig(): EntityConfig
    {
        return $this->entityConfig;
    }

    /**
     * @return array
     */
    public function getRelations(): array
    {
        return $this->relations;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    public function addRelation(Relation $relation): self
    {
        $this->relations[] = $relation;
        return $this;
    }

    public function addField(string $title, FieldType $fieldType, FieldConfig $config = null): self
    {
        $config = ($config === null) ? new FieldConfig() : $config;

        $this->fields[] = new Field($title, $fieldType, $config);

        return $this;
    }

    public function addDatabaseConfig(DatabaseConfig $config): self
    {
        $this->databaseConfig = $config;
        return $this;
    }

    public function addEntityConfig(EntityConfig $config): self
    {
        $this->entityConfig = $config;
        return $this;
    }

    public function addTableConfig(TableConfig $config): self
    {
        $this->tableConfig = $config;
        return $this;
    }
}