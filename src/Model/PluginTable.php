<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model;

class PluginTable extends TableBase
{
    /**
     * @var string
     */
    private $pluginTitle;

    /**
     * @return mixed
     */
    public function getPluginTitle()
    {
        return $this->pluginTitle;
    }

    /**
     * @param mixed $pluginTitle
     */
    public function setPluginTitle($pluginTitle)
    {
        $this->pluginTitle = $pluginTitle;
    }

    /**
     * PluginTable constructor.
     * @param string $title
     * @param string $pluginTitle
     */
    public function __construct(string $title, string $pluginTitle)
    {
        parent::__construct($title);
        $this->setPluginTitle($pluginTitle);
    }
}