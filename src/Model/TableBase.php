<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model;

class TableBase
{
    /**
     * @var string
     */
    private $title;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function __construct(string $title)
    {
        $this->title = $title;
    }
}