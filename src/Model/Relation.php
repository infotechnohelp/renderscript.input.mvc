<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model;

use RenderScript\Input\MVC\Model\Relation\Config;
use RenderScript\Input\MVC\Model\Relation\Type;

class Relation
{
    /**
     * @var TableBase
     */
    private $table;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var Config|null
     */
    private $config;

    public function __construct(TableBase $table, Type $type, Config $config = null)
    {
        $this->table = $table;
        $this->type = $type;
        $this->config = $config;
    }

    /**
     * @return TableBase
     */
    public function getTable(): TableBase
    {
        return $this->table;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }


}