<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Rule;

class Factory
{
    public function requireOnlyOneOfGroups(array $data)
    {
        return new RequireOnlyOneOfGroupsRule($data);
    }
}