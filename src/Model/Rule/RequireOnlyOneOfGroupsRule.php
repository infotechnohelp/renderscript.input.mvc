<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Rule;

use RenderScript\Input\MVC\Model\Rule;

class RequireOnlyOneOfGroupsRule extends Rule
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }
}