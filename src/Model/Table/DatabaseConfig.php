<?php /** @noinspection PhpPrivateFieldCanBeLocalVariableInspection */

namespace RenderScript\Input\MVC\Model\Table;

class DatabaseConfig
{
    /**
     * @var array
     */
    private $uniqueFieldGroups = [];

    public function uniqueFieldGroups(array $value): self
    {
        $this->uniqueFieldGroups = $value;
        return $this;
    }
}