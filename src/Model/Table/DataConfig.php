<?php /** @noinspection PhpPrivateFieldCanBeLocalVariableInspection */

namespace RenderScript\Input\MVC\Model\Table;

class DataConfig
{
    /**
     * @var bool
     */
    private $constantContainer = false;

    /**
     * @var string|null
     */
    private $seedField = null;

    /**
     * @var array
     */
    private $seeds = [];

    public function seeds(array $value): self
    {
        $this->seeds = $value;
        return $this;
    }

    public function seedField(string $value): self
    {
        $this->seedField = $value;
        return $this;
    }

    public function constantContainer(bool $value = true): self
    {
        $this->constantContainer = $value;
        return $this;
    }
}