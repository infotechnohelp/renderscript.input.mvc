<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Table;

/**
 * Class EntityConfig
 * @package RenderScript\Input\MVC\Model\Table
 */
class EntityConfig
{
    /**
     * @var bool
     */
    private $timestampBehaviour = true;

    /**
     * @var DataConfig
     */
    private $data;

    public function __construct()
    {
        $this->data = new DataConfig();
    }

    /**
     * @return bool
     */
    public function isTimestampBehaviour(): bool
    {
        return $this->timestampBehaviour;
    }

    public function timestampBehaviour(bool $value = true): self
    {
        $this->timestampBehaviour = $value;
        return $this;
    }

    public function data(DataConfig $value): self
    {
        $this->data = $value;
        return $this;
    }
}