<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Table;

use RenderScript\Input\MVC\Model\Rule;
use RenderScript\Input\MVC\Model\Validation;

class TableConfig
{
    /**
     * @var array
     */
    private $validations = [];

    /**
     * @var array
     */
    private $rules = [];


    public function addValidation(Validation $validation): self
    {
        $this->validations[] = $validation;

        return $this;
    }

    public function addRule(Rule $rule): self
    {
        $this->rules[] = $rule;

        return $this;
    }
}