<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Table;

use RenderScript\Input\MVC\Model\Field\Config as FieldConfig;
use RenderScript\Input\MVC\Model\Relation\Config as RelationConfig;

class TableConfigFactory
{
    public function Table()
    {
        return new TableConfig();
    }

    public function Entity()
    {
        return new EntityConfig();
    }

    public function Data()
    {
        return new DataConfig();
    }

    public function Database()
    {
        return new DatabaseConfig();
    }

    public function Relation()
    {
        return new RelationConfig();
    }

    public function Field()
    {
        return new FieldConfig();
    }
}