<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model;

use RenderScript\Input\MVC\Model\Field\Config;
use RenderScript\Input\MVC\Model\Field\Type;

class Field
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Type
     */
    private $type;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return Type
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * @return Config
     */
    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * Field constructor.
     * @param string $title
     * @param Type $type
     * @param Config $config
     */
    public function __construct(string $title, Type $type, Config $config)
    {
        $this->title = $title;
        $this->type = $type;
        $this->config = $config;
    }
}