<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Relation;

use RenderScript\Input\MVC\Model\Relation;
use RenderScript\Input\MVC\Model\TableBase;
use RenderScript\Input\MVC\Model\Relation\Type\BelongsTo;

class BelongsToRelation extends Relation
{
    public function __construct(TableBase $table, Config $config)
    {
        parent::__construct($table, new BelongsTo(), $config);
    }
}