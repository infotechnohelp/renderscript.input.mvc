<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Relation;

use RenderScript\Input\MVC\Model\Relation\Type\ManyToMany;

class Type
{
    public static function manyToMany()
    {
        return new ManyToMany();
    }
}