<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Relation;

use RenderScript\Input\MVC\Model\Relation;
use RenderScript\Input\MVC\Model\TableBase;
use RenderScript\Input\MVC\Model\Relation\Type\ManyToMany;

class ManyToManyRelation extends Relation
{
    public function __construct(TableBase $table, Config $config = null)
    {
        parent::__construct($table, new ManyToMany(), $config);
    }
}