<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Relation;

use RenderScript\Input\MVC\Model\TableBase;

class Factory
{
    public function oneToMany(TableBase $table, Config $config = null)
    {
        return new OneToManyRelation($table, $config);
    }

    public function manyToMany(TableBase $table, Config $config = null)
    {
        return new ManyToManyRelation($table, $config);
    }

    public function belongsTo(TableBase $table, Config $config)
    {
        return new BelongsToRelation($table, $config);
    }

    public function hasMany(TableBase $table, Config $config)
    {
        return new HasManyRelation($table, $config);
    }
}