<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Relation;

use RenderScript\Input\MVC\Model\Relation;
use RenderScript\Input\MVC\Model\TableBase;
use RenderScript\Input\MVC\Model\Relation\Type\HasMany;

class HasManyRelation extends Relation
{
    public function __construct(TableBase $table, Config $config)
    {
        parent::__construct($table, new HasMany(), $config);
    }
}