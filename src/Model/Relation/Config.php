<?php /** @noinspection PhpPrivateFieldCanBeLocalVariableInspection */

namespace RenderScript\Input\MVC\Model\Relation;

class Config
{
    /**
     * @var bool
     */
    private $nullable = false;

    /**
     * @var string|null
     */
    private $title = null;

    /**
     * @var string|null
     */
    private $foreignKey = null;

    public function nullable(bool $value = true): self
    {
        $this->nullable = $value;
        return $this;
    }

    public function title(string $value): self
    {
        $this->title = $value;
        return $this;
    }

    public function foreignKey(string $value): self
    {
        $this->foreignKey = $value;
        return $this;
    }

}