<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Relation\Type;

use RenderScript\Input\MVC\Model\Relation\Type;

class ManyToMany extends Type
{
    /**
     * @var string
     */
    private $type = 'manyToMany';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}