<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Relation\Type;

use RenderScript\Input\MVC\Model\Relation\Type;

class HasMany extends Type
{
    /**
     * @var string
     */
    private $type = 'hasMany';

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
}