<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Validation;

use RenderScript\Input\MVC\Model\Validation;

class MinLengthValidation extends Validation
{
    /**
     * @var int
     */
    private $length;

    public function __construct(int $length)
    {
        $this->length = $length;
    }
}