<?php declare(strict_types=1);

namespace RenderScript\Input\MVC\Model\Validation;

class Factory
{
    public function minLength(int $length)
    {
        return new MinLengthValidation($length);
    }
}