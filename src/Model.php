<?php declare(strict_types=1);

namespace RenderScript\Input\MVC;

use RenderScript\Input\MVC\Model\PluginTable;
use RenderScript\Input\MVC\Model\Table;

class Model
{
    /**
     * @var array
     */
    private static $tables = [];

    public static function getTables(): array
    {
        return self::$tables;
    }

    public static function pluginTable(string $title, string $pluginTitle): PluginTable
    {
        return new PluginTable($title, $pluginTitle);
    }

    public static function newTable(string $title): Table
    {
        $table = new Table($title);
        self::$tables[] = $table;
        return $table;
    }
}
